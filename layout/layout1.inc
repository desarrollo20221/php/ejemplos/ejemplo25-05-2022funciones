<?php
require_once './libreria.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?= css(); ?>
        <title><?= $titulo ?></title>
    </head>
    <body>
        <?= menu() ?>
        <?= contenido($datos) ?>
        <?= js(); ?>
    </body>
</html>

